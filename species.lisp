;;;; species.lisp -- Character species.
;;;;
;;;; $Id: species.lisp,v 1.1 2003/12/23 13:06:29 james Exp $
;;;;
;;;; Copyfnord (K) 3167 - 3169 James A. Crippen <james@unlambda.com>
;;;;
;;;; This software is in the Public Domain. If you break it you get to keep
;;;; both pieces.
;;;;
;;;; Various species of characters.

(defclass species (attributes-mixin)
  ())

(defclass species-human (species)
  ((base-attributes :initform (make-instance 'attributes :list (make-attributes-with-values
                                                                (roll-human-attributes))))))

(defun roll-human-attributes ()
  (loop for a in *attribute-types*
        collect (cons a (car (roll-dice 1 20)))))
