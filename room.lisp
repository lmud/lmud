;;;; room.lisp -- Rooms.
;;;;
;;;; $Id: $
;;;;
;;;; Copyfnord (K) 3167 - 3169 James A. Crippen <james@unlambda.com>
;;;;
;;;; This software is in the Public Domain. If you break it you get to keep
;;;; both pieces.
;;;;
;;;; Rooms are places where things may be located. They are not things in and
;;;; of themselves, but may contain things.

(in-package #:lmud-internals)

(defclass room (id)
  (
