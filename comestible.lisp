;;;; comestible.lisp -- Comestible substances.
;;;;
;;;; $Id: blank.lisp,v 1.1 2003/12/23 13:06:29 james Exp $
;;;;
;;;; Copyfnord (K) 3167 - 3169 James A. Crippen <james@unlambda.com>
;;;;
;;;; This software is in the Public Domain. If you break it you get to keep
;;;; both pieces.
;;;;
;;;; Mmm, yum.

(in-package #:lmud-internals)

(defconstant +max-hunger+ 100
  "The maximum amount of hunger a character may have.")

(defclass hunger-mixin ()
  ((hunger :accessor hunger :initform (1- +max-hunger+) :initarg :hunger)
   (energy :accessor energy :initform 0 :initarg :energy)))
