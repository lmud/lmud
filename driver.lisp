;;;; driver.lisp -- LMud driver.
;;;;
;;;; $Id: driver.lisp,v 1.2 2003/12/24 11:38:39 james Exp $
;;;;
;;;; Copyfnord (K) 3167 - 3169 James A. Crippen <james@unlambda.com>
;;;;
;;;; This software is in the Public Domain. If you break it you get to keep
;;;; both pieces.
;;;;
;;;; The LMud driver. In MUD parlance a 'driver' is the software engine that
;;;; runs the MUD and in whose programming or configuration language language
;;;; the MUD is written. Thus this is the top level of the LMud system.

(in-package #:lmudi)

(defun boot-lmud ()
  "The initial function that starts an LMud system."
  (init-universe)
  (spawn-listener-thread)
  (values))

(defun init-universe ()
  ())

(defun spawn-listener-thread ()
  ())


  ;;
;;;;;; The Listener.
  ;;

(defun listener ()
  ())


(define-condition lmud-quit-condition

  ;;
;;;;;; Locks
  ;;

(defclass locks-mixin ()
  ((write-lock :reader write-lock-p :initform nil)
   (read-lock :reader read-lock-p :initform nil)))
