;;;; thing.lisp -- Things.
;;;;
;;;; $Id: $
;;;;
;;;; Copyfnord (K) 3167 - 3169 James A. Crippen <james@unlambda.com>
;;;;
;;;; This software is in the Public Domain. If you break it you get to keep
;;;; both pieces.
;;;;
;;;; Things are a protocol class for rooms, objects, and characters. Various
;;;; thing subclasses supply appropriate properties for mobility, liveness,
;;;; and so forth.

(in-package #:lmud-internals)

(defclass thing (id noun-mixin description-mixin location-mixin)
  ((noun :initform "thing")
   (description :initform "It is a thing.")
   (movable :reader movablep :initform t)
   (animate :reader animatep :initform nil)
   (alive :reader alivep :initform nil))
  (:documentation
   "THING is the protocol class for all objects, rooms, characters, etc in the
Mud. A thing by default is movable but inanimate, and not alive."))

(defclass immovable-thing (thing)
  ((noun :initform "immovable thing")
   (description :initform "It seems to be impossible to move.")
   (movable :initform nil))
  (:documentation
   "An IMMOVABLE-THING is a thing whose location is fixed and may not be moved
during the game without surgery."))

(defclass animate-thing (thing)
  ((noun :initform "animate thing")
   (description :initform "It seems like it could move of its own accord.")
   (movable :initform t)
   (animate :initform t))
  (:documentation
   "An ANIMATE-THING is a thing which may change its location of its own
accord. This does not necessarily indicate that the thing is living, for
example an automobile might be considered animate because it can change its
own location, yet it is not alive."))

(defclass inanimate-thing (thing)
  ((noun :initform "inanimate thing")
   (description :initform "It doesn't seem to move of its own accord.")
   (movable :initform t)
   (animate :initform nil))
  (:documentation
   "An INANIMATE-THING is a thing which is incapable of changing its own
location. It must be moved by some other thing."))

(defclass living-thing (thing)
  ((noun :initform "living thing")
   (description :initform "It is obviously alive.")
   (alive :initform t)
   (age :accessor age
        :initform 0
        :type (integer 0 *))))
  (:documentation
   "A LIVING-THING is a thing which is alive, has an age, and may die. A tree
is typically a living thing, yet it is not animate because it cannot move of
its own accord."))

(defclass nonliving-thing (thing)
  ((noun :initform "nonliving thing")
   (description :initform "It isn't alive.")
   (alive :initform nil))
  (:documentation
   "A NONLIVING-THING is a thing which is not alive. A rock might be a nonliving
thing, as might a book. A would not ordinarily be a nonliving thing because it
can grow and die."))

(defgeneric look-at (it))

(defmethod look-at ((it thing) stream)
  (format stream "It is a ~A." (noun it)))

(defgeneric examine (it))

(defmethod examine ((it thing) stream)
  (format stream "You see a ~A.~%~A" (noun it) (description it)))
