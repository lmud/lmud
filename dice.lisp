;;;; dice.lisp -- Randomness.
;;;;
;;;; $Id: blank.lisp,v 1.1 2003/12/23 13:06:29 james Exp $
;;;;
;;;; Copyfnord (K) 3167 - 3169 James A. Crippen <james@unlambda.com>
;;;;
;;;; This software is in the Public Domain. If you break it you get to keep
;;;; both pieces.
;;;;
;;;; Random value generation infrastructure (dice rolling code).

(in-package #:lmud-internals)

(defun roll-dice (rolls sides)
  (declare (type (integer 0 *) rolls sides))
  "Roll ROLLS dice of SIDES sides. Returns the result of the rolls as a
list of positive integers."
  (loop for i from 1 to rolls
        collect (random sides)))
