;;;; commands.lisp -- Command processing.
;;;;
;;;; $Id: $
;;;;
;;;; Copyfnord (K) 3167 - 3169 James A. Crippen <james@unlambda.com>
;;;;
;;;; This software is in the Public Domain. If you break it you get to keep
;;;; both pieces.
;;;;
;;;; Command line interface, commands, command tables, and command processing.
;;;; Much of the general design is derived from CLIM but without any menus or
;;;; presentations, which are meaningless on a text terminal.

(in-package #:lmud-internals)


  ;;
;;;;;; Command Tables.
  ;;

(defvar *command-tables* (make-hash-table :test #'eq))

(defclass command-table ()
  ((name :reader  command-table-name
         :initarg :name)
   (inherit-from :reader   command-table-inherit-from
                 :initarg  :inherit-from
                 :initform '()
                 :documentation "List of command tables which this table inherits from.")
   (commands :initform (make-hash-table :test #'equal)
             :documentation "Hash table of commands keyed by command-name.")
   (command-line-names :initform (make-hash-table :test #'equal)
                       :documentation "Hash table of commands keyed by command-line-name.")
   (command-abbrevs :initform (make-hash-table :test #'equal)
                    :documentation "Hash table of commands keyed by elements of command-abbrevs.")))

(defmethod print-object ((comtab command-table) stream)
  (print-unreadable-object (comtab stream :identity t :type t)
    (format stream "~S" (command-table-name comtab))))

(define-condition command-table-error (error) ())
(define-condition command-table-not-found (command-table-error) ())
(define-condition command-table-already-exists (command-table-error) ())
(define-condition command-not-present (command-table-error) ())
(define-condition command-not-accessible (command-table-error) ())
(define-condition command-already-present (command-table-error) ())

(defun find-command-table (name &key (errorp t))
  (cond ((command-table-p name) name)
        ((gethash name *command-tables*))
        (errorp (error 'command-table-not-found))
        (t nil)))

(defun make-command-table (name &key inherit-from (errorp t))
  (if (and name errorp (gethash name *command-tables*))
      (error 'command-table-already-exists)
      (let ((new-table (make-instance 'command-table
                                      :name name
                                      :inherit-from inherit-from)))
        (when name
          (setf (gethash name *command-tables*) new-table))
        new-table)))

;;; The global command table is always available and always has something in
;;; it. But what it has in it has yet to be determined. Note that this doesn't
;;; contain all the commands that exist, just ones which are available
;;; globally. Having a command table for all commands is a Bad Thing because
;;; then you couldn't define two different commands with the same name but
;;; different functionality.
(make-command-table 'global-command-table)

(defmacro define-command-table (name &key (inherit-from '(global-command-table)))
  `(let ((old-table (gethash ',name *command-tables* nil)))
    (if old-table
        (with-slots (inherit-from) old-table
          (setq inherit-from ',inherit-from)
          old-table)
        (make-command-table ',name
                            :inherit-from ',inherit-from
                            :errorp nil))))

(defun add-command-to-command-table (command command-table &key (errorp t))
  (declare (type command command))
  (let ((table (find-command-table command-table))
        (name (command-name com))
        (line-name (command-line-name com))
        (abbrevs (command-line-abbrevs com)))
    (setf (gethash name (commands table)) command)
    (setf (gethash line-name (slot-value table 'command-line-names)) command)
    (mapcar #'(lambda (abv)
                (setf (gethash abv (slot-value table 'command-abbrevs)) command))
            (command-abbrevs command))))))

(defun remove-command-from-command-table (command command-table &key (errorp t))
  (declare (type (or string symbol command) command))
  (let* ((table (find-command-table command-table))
         (com (typecase command
                (string (gethash command-name (commands table)))
                (symbol (gethash (command-name-from-symbol command) (commands table)))
                (command command))))
    (if (null com)
        (when errorp
          (error 'command-not-present))
        (progn
          (remhash (command-name com) (slot-value table 'commands))
          (remhash (command-line-name com) (slot-value table 'command-line-names))
          (mapcar #'(lambda (abv)
                      (remhash abv (slot-value table 'command-abbrevs)))
                  (command-abbrevs table))))))

(defun apply-with-command-table-inheritance (func command-table)
  (funcall func command-table)
  (mapc #'(lambda (inherited-command-table)
            (apply-with-command-table-inheritance fun (find-command-table inherited-command-table)))
        (command-table-inherit-from command-table)))

;;; FIXME: Finish.
(defmacro do-command-table-inheritance (command-table-var command-table &body body))

(defun map-over-command-table-commands (func command-table &key (inherited t))
  (let ((table (find-command-table command-table)))
    (flet ((map-func (tab)
             (maphash #'(lambda (key val)
                          (declare (ignore val))
                          (funcall func key))
                      (slot-value tab 'commands))))
      (if inherited
          (apply-with-command-table-inheritance #'map-func table)
          (map-func table)))))

(defun map-over-command-table-names (func command-table &key (inherited t))
  (let ((table (find-command-table command-table)))
    (flet ((map-func (tab)
             (maphash func (slot-value table 'command-line-names))))
      (if inherited
          (apply-with-command-table-inheritance #'map-func command-table)
          (map-func command-table)))))

(defun map-over-command-table-abbrevs (func command-table &key (inherited t))
  (let ((table (find-command-table command-table)))
    (flet ((map-func (tab)
             (maphash func (slot-value table 'command-line-abbrevs))))
      (if inherited
          (apply-with-command-table-inheritance #'map-func command-table)
          (map-func command-table)))))

;;; FIXME: Finish.
(defun command-present-in-command-table-p (command-name command-table))

;;; FIXME: Finish.
(defun find-command-from-command-line-name (name command-table &key (errorp t)))

;;; FIXME: Finish.
(defun command-line-name-for-command (command-name command-table &key (errorp t)))


  ;;
;;;;;; Commands.
  ;;

(defclass command ()
  ((command-name :accessor command-name
                 :initarg  :name
                 :initform "")
   (command-line-name :accessor command-line-name
                      :initarg  :command-line-name
                      :initform "")
   (command-function :accessor command-function :initarg :function)
   (command-arguments :accessor command-arguments :initarg :arguments)
   (command-abbrevs :accessor command-abbrevs :initarg :abbrevs)
   (command-documentation :reader  command-documentation
                          :initarg :documentation
                          :initform "")))

(defmethod print-object ((com command) stream)
  (print-unreadable-object (com stream :identity t :type t)
    (cond ((slot-boundp com 'command-name)
           (format stream "~A" (command-name com)))
          (t nil))))

(defun command-name-from-symbol (symbol)
  (let ((name (symbol-name symbol)))
    ;; In CLIM commands are Capitalized but in LMud they're all lowercase like
    ;; Unix commands. This makes them easier to type quickly on poor terminals.
    (string-downcase (substitute #\Space #\-
                                   (subseq name (string/= "COM-" name))))))

(defmacro defcommand (name-and-options args &body body)
  "Define a new command. NAME-AND-OPTIONS is a list of the command function's
name and options for the definition of the command. ARGS is the list of
argument lists.  BODY is the body of the command, within which the arguments
defined in ARGS are available.

FIXME: Need much better docs for defcommand."
  (unless (listp name-and-options)
    (setf name-and-options (list name-and-options)))
  ;; Parse out the name-and-options like a real lambda list.
  (destructuring-bind (func &key command-table name abbrev)
      name-and-options
    (multiple-value-bind (required-args optional-args)
        ;; Walk args list until we hit end or beginning of optional args, and
        ;; return lists of each, the latter excluding the &optional keyword.
        (loop for arg-tail on args
              for (arg) = arg-tail
              until (eq arg '&optional)
              collect arg into required
              finally (return (values required (cdr arg-tail))))
      ;; Build the command function's argument list from the supplied args.
      (let ((command-function-arglist
             `(,@(mapcar #'car required-args)
               ,@(and optional-args
                      `(&optional ,@(mapcar #'(lambda (arg-clause)
                                                (destructuring-bind (arg-name ptype
                                                                     &key default
                                                                     &allow-other-keys)
                                                    arg-clause
                                                  (declare (ignore ptype))
                                                  `(,arg-name ,default)))
                                            keyword-args))))))
        ;; Create the function.
        `(progn
          (defun ,func ,command-function-arglist
            ,@body)
          ;; Add it to the specified command table.
          ,(if command-table
               `(add-command-to-command-table ',func ',command-table
                                              :name ,name
                                              :abbrev ,abbrev
                                              :errorp nil)))
          ',func)))))
