;;;; attributes.lisp -- Attributes for living things.
;;;;
;;;; $Id: attributes.lisp,v 1.2 2003/12/24 11:36:38 james Exp $
;;;;
;;;; Copyfnord (K) 3167 - 3169 James A. Crippen <james@unlambda.com>
;;;;
;;;; This software is in the Public Domain. If you break it you get to keep
;;;; both pieces.
;;;;
;;;; Attributes for living things, eg hit points, mana, etc. Adding anything
;;;; that doesn't behave like an instance of the ATTRIBUTE class will be a
;;;; real PITA to make fit in with the rest.

(in-package #:lmud-internals)

(defconstant +default-attribute-value+ 1)

(defvar *attributes* '(make-hash-table))

(defun attributes ()
  (loop for i being each hash-key of *attributes*
        collect i))

(deftype attribute-type ()
  `(or nil (member ,@(attributes))))

(defun define-attribute (type &optional abbrev)
  (let ((name (symbol-name type))
        (abv (if (and (not (null abbrev)) (stringp abbrev))
                 abbrev
                 (subseq name 0 3))))
    (setf (gethash type *attributes*) (list name abv))))

(define-attribute experience "exp")
(define-attribute hitpoints "hp")
(define-attribute mana "mp")
(define-attribute strength "str")
(define-attribute dexterity "dex")
(define-attribute intelligence "int")
(define-attribute wisdom "wis")
(define-attribute stamina "sta")
(define-attribute charisma "chr")
(define-attribute attack "att")
(define-attribute defense "def")

(defclass attribute ()
  ((type :type attribute-types :initarg :type :initform nil :accessor attribute-type)
   (name :type string :initarg :name :initform "" :accessor attribute-name)
   (abbrev :type string :initarg :abbrev :initform "" :accessor attribute-abbrev)
   (value :type (integer 0 *) :initarg :value :initform 0 :accessor attribute-value)))

(defun make-attribute (type value)
  (let ((attrib (gethash type *attributes*)))
    (make-instance 'attribute
                   :type type
                   :name (car attrib)
                   :abbrev (cadr attrib)
                   :value value)))

(defun make-attributes-list ()
  (list (loop for a in (attributes)
              (make-attribute a +default-attribute-value+))))

(defclass attributes-mixin ()
  ((base-attributes :initarg :base-attributes :accessor base-attributes :initform nil)
   (current-attributes :initarg :current-attributes :accessor current-attributes :initform nil)
   (max-attributes :initarg :max-attributes :accessor max-attributes :initform nil)
   (attribute-mods :initarg :attribute-mods :accessor attribute-mods :initform nil)))

(defmethod initialize-instance :after ((am attributes-mixin))
  (with-slots ((base base-attributes) (current current-attributes)
               (max max-attributes) (mod mod-attributes))
      am
    (if (null base) (setf base (make-attributes-list)))
    (if (null current) (setf current (copy-attributes-list base)))
    (if (null max) (setf max (copy-attributes-list base)))))
