;;;; comdefs.lisp -- Definitions of basic commands.
;;;;
;;;; $Id: $
;;;;
;;;; Copyfnord (K) 3167 - 3169 James A. Crippen <james@unlambda.com>
;;;;
;;;; This software is in the Public Domain. If you break it you get to keep
;;;; both pieces.
;;;;
;;;; Basic commands for the user interface.

(in-package #:lmud)

(defcommand (com-example :name "example-command"
                         :command-table global-command-table
                         :abbrev "eg")
    ((arg1 (or symbol string)
           :default "1"
           :prompt  "argument-1")
     &optional
     (arg2 (or symbol string)
           :default "2"
           :prompt  "argument-2"
           :supplied arg2-supplied-p))
  (format t "This is an example command.~%Argument 1 was: \"~A\"~%" arg1)
  (if arg2-supplied-p (format t "Argument 2 was: \"~A\"" arg2))

