;;;; living.lisp -- Living things.
;;;;
;;;; $Id: $
;;;;
;;;; Copyfnord (K) 3167 - 3169 James A. Crippen <james@unlambda.com>
;;;;
;;;; This software is in the Public Domain. If you break it you get to keep
;;;; both pieces.
;;;;
;;;; Living things such as animals, plants, etc.

(in-package #:lmud-internals)

(defclass animal (living-thing animate-thing)
  ())

(defclass plant (living-thing inanimate-thing)
  ())

(defclass tree (plant immovable-thing)
  ())
