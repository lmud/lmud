;;;; mixins.lisp -- Various mixins.
;;;;
;;;; $Id: $
;;;;
;;;; Copyfnord (K) 3167 - 3169 James A. Crippen <james@unlambda.com>
;;;;
;;;; This software is in the Public Domain. If you break it you get to keep
;;;; both pieces.
;;;;
;;;; Assorted classes supplying mixins that yield added flavor.

(in-package #:lmud-internals)


  ;;
;;;;;; Locations.
  ;;

(defclass location-mixin ()
  ((location :type (or room nil)
             :initform nil
             :initarg :location
             :accessor location)))


  ;;
;;;;;; Nouns and Descriptions.
  ;;

(defclass noun-mixin ()
  ((noun :type     string
         :initform "<nil>"
         :initarg  :noun
         :accessor noun))
  (:documentation
   "A NOUN is a short (one or two word) description of something. It is very
often a single noun as the name implies, or perhaps a noun and an adjective."))

(defmethod print-object ((p pname-mixin) stream)
  (print-unreadable-object (p stream :type t :identity t)
    (princ (pname p) stream)))

(defclass description-mixin ()
  ((description :accessor description
                :initarg  :description
                :initform "<No description.>"
                :type     string))
  (:documentation
   "A DESCRIPTION is a sentential description of something. It usually consists
of a single sentence starting with \"It is\" or \"The <thing> is\"."))

(defmethod print-object ((d description-mixin) stream)
  (print-unreadable-object (d stream :type t :identity t)
    (let ((dstr (description d)))
      (if (> 8 (length dstr))
          (princ (concat (subseq dstr 0 8) "..."))
          (princ dstr)))))


  ;;
;;;;;; Unique Identifiers.
  ;;

;;; IDs are magic cookies! Don't use them for anything except uniqueness
;;; comparisons with other IDs. Don't try to track a particular object by
;;; storing its ID somewhere because the ID may change if the object is
;;; unloaded and reloaded, eg by game save and restore.

(defclass id ()
  ((id :type (integer 0 *) :initform 0 :reader id)
   (%max-id :type (integer 0 *) :initform 0 :allocation :class))

;;; The ID 0 is an invalid ID. It should never exist since the
;;; INITIALIZE-INSTANCE method below is supposed to replace it with the
;;; current %MAX-ID.

(defmethod initialize-instance :after ((i id) &key &allow-other-keys)
  (if (eql (id i) 0)
      (setf (slot-value i 'id) (incf (slot-value i '%max-id)))))

(defmethod print-object ((i id) stream)
  (print-unreadable-object (p stream :type t :identity t)
    (princ (id i) stream)))
