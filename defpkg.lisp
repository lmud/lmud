;;;; defpkg.lisp -- Packaging.
;;;;
;;;; $Id: $
;;;;
;;;; Copyfnord (K) 3167 - 3169 James A. Crippen <james@unlambda.com>
;;;;
;;;; This software is in the Public Domain. If you break it you get to keep
;;;; both pieces.
;;;;
;;;; The packaging for LMud.

(defpackage #:lmud-internals
  (:nickname #:lmudi)
  (:use #:cl #:split-sequence)

(defpackage #:lmud
  (:use #:cl #:lmud-internals)
  (:export #:boot-lmud))
