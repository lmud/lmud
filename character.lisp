;;;; character.lisp -- Characters.
;;;;
;;;; $Id: character.lisp,v 1.1 2003/12/23 13:06:29 james Exp $
;;;;
;;;; Copyfnord (K) 3167 - 3169 James A. Crippen <james@unlambda.com>
;;;;
;;;; This software is in the Public Domain. If you break it you get to keep
;;;; both pieces.
;;;;
;;;; Characters are both players and NPCs.

(in-package #:lmud-internals)

(defclass character (animate-thing living-thing attributes-mixin inventory-mixin
                     hunger-mixin)
  ((noun :initform "character")
   (description :initform "A character.")
   (name :accessor name
         :initform "<nil>"
         :initarg :name)
   (killable :reader killablep
             :initarg :killable
             :initform t)
   ()
  (:documentation
   "A CHARACTER is an animate and living thing which is killable and has stats,
a species, and an alignment."))

(defclass nonplayer (character)
  ((noun :initform "nonplayer")
   (description :initform "A nonplayer character."))
  (:documentation
   "A NONPLAYER, commonly known as an NPC, is a character which is controlled
by the MUD and not by a player in the Real World."))

(defclass player (character)
  ((noun :initform "player")
   (description :initform "A player."))
  (:documentation
   "A PLAYER is a character which is controlled by a person in the Real World."))
