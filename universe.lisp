;;;; universe.lisp -- The Universe.
;;;;
;;;; $Id: $
;;;;
;;;; Copyfnord (K) 3167 - 3169 James A. Crippen <james@unlambda.com>
;;;;
;;;; This software is in the Public Domain. If you break it you get to keep
;;;; both pieces.
;;;;
;;;; The universe is the top level container for all objects in the Mud.

(in-package #:lmud-internals)

(defparameter *universe-estimations*
  '((rooms . 100) (characters . 100) (objects . 100)))

(defclass universe (id)
  ((rooms :accessor universe-rooms
          :initarg  :rooms
          :initform (make-hash-table :test #'eql :size (cdr (assoc 'rooms *universe-estimations*))))
   (characters :accessor universe-characters
               :initarg  :characters
               :initform (make-hash-table :test #'eql :size (cdr (assoc 'characters *universe-estimations*))))
   (objects :accessor universe-objects
            :initarg  :objects
            :initform (make-hash-table :test #'eql :size (cdr (assoc 'objects *universe-estimations*))))))

(defgeneric add-to-universe (u obj)
  "Add a thing OBJ to a universe U.")

(defmethod add-to-universe ((u universe) (r room))
  (setf (gethash (id r) (universe-rooms u)) r))

(defmethod add-to-universe ((u universe) (c character))
  (setf (gethash (id c) (universe-characters u)) c))

(defmethod add-to-universe ((u universe) (o object))
  (setf (gethash (id o) (universe-objects u)) o))

(defgeneric remove-from-universe (u obj)
  "Remove a thing OBJ from a universe U.")

(defmethod remove-from-universe ((u universe) (r room))
  (remhash (id r) (universe-rooms u)))

(defmethod remove-from-universe ((u universe) (c character))
  (remhash (id c) (universe-rooms u)))

(defmethod remove-from-universe ((u universe) (o object))
  (remhash (id o) (universe-rooms u)))

(defun make-universe (&key rooms objects characters)
  (let ((univ (make-instance 'universe)))
    (cond ((not (null rooms))
           (add-to-universe univ rooms))
          ((not (null objects))
           (add-to-universe univ objects))
          ((not (null characters))
           (add-to-unvierse univ characters)))))

(defmethod save-universe ((u universe) file)
  ())
